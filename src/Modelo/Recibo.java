
package Modelo;


public class Recibo {
    
    private int tipoServicio, numRecibo, kilowatConsumido;
    private String fecha, nombre, domicilio;
    private float costoKilowat;

    //CONSTRUCTORES
    
    public Recibo(int tipoServicio, int numRecibo, int kilowatConsumido, String fecha, String nombre, String domicilio, float costoKilowat) {
        this.tipoServicio = tipoServicio;
        this.numRecibo = numRecibo;
        this.kilowatConsumido = kilowatConsumido;
        this.fecha = fecha;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.costoKilowat = costoKilowat;
    }

    public Recibo(Recibo ob) {
        this.tipoServicio = ob.tipoServicio;
        this.numRecibo = ob.numRecibo;
        this.kilowatConsumido = ob.kilowatConsumido;
        this.fecha = ob.fecha;
        this.nombre = ob.nombre;
        this.domicilio = ob.domicilio;
        this.costoKilowat = ob.costoKilowat;
    }

    public Recibo() {
        this.tipoServicio = 0;
        this.numRecibo = 0;
        this.kilowatConsumido = 0;
        this.fecha = "";
        this.nombre = "";
        this.domicilio = "";
        this.costoKilowat = 0;
    }

    //SETS Y GETS
    
    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getKilowatConsumido() {
        return kilowatConsumido;
    }

    public void setKilowatConsumido(int kilowatConsumido) {
        this.kilowatConsumido = kilowatConsumido;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public float getCostoKilowat() {
        return costoKilowat;
    }

    public void setCostoKilowat(float costoKilowat) {
        this.costoKilowat = costoKilowat;
    }
    
    public float calcularSubtotal(){
        return (this.costoKilowat*this.kilowatConsumido);
    }
    
    public float calcularImpuesto(){
        return (calcularSubtotal()*0.16f);
    }
    
    public float calcularTotal(){
        return (calcularSubtotal()+calcularImpuesto());
    }
    
    
    
}
