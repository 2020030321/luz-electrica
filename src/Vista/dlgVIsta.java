/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package Vista;

/**
 *
 * @author elche
 */
public class dlgVIsta extends javax.swing.JDialog {

    /**
     * Creates new form dlgVIsta
     */
    public dlgVIsta(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtDomicilio = new javax.swing.JTextField();
        btnMostrar = new javax.swing.JButton();
        txtCostoKilo = new javax.swing.JTextField();
        btnCancelar = new javax.swing.JButton();
        txtKiloConsumido = new javax.swing.JTextField();
        btnLimpiar = new javax.swing.JButton();
        cboTipoServicio = new javax.swing.JComboBox<>();
        btnCerrar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        lblSubtotal = new javax.swing.JLabel();
        lblImpuesto = new javax.swing.JLabel();
        lblTotalPago = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtFecha = new javax.swing.JTextField();
        btnGuarda = new javax.swing.JButton();
        txtNombre = new javax.swing.JTextField();
        btnNuevo = new javax.swing.JButton();
        txtNumRecibo = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        txtDomicilio.setEnabled(false);
        getContentPane().add(txtDomicilio);
        txtDomicilio.setBounds(130, 90, 327, 24);

        btnMostrar.setText("Mostrar");
        btnMostrar.setEnabled(false);
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(470, 120, 90, 41);

        txtCostoKilo.setEnabled(false);
        getContentPane().add(txtCostoKilo);
        txtCostoKilo.setBounds(360, 130, 98, 24);

        btnCancelar.setText("Cancelar");
        btnCancelar.setEnabled(false);
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(190, 350, 86, 24);

        txtKiloConsumido.setEnabled(false);
        getContentPane().add(txtKiloConsumido);
        txtKiloConsumido.setBounds(170, 170, 94, 24);

        btnLimpiar.setText("Limpiar");
        btnLimpiar.setEnabled(false);
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(40, 350, 86, 24);

        cboTipoServicio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione...", "Residencial", "Comercial", "Industrial", " " }));
        cboTipoServicio.setEnabled(false);
        cboTipoServicio.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboTipoServicioItemStateChanged(evt);
            }
        });
        cboTipoServicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTipoServicioActionPerformed(evt);
            }
        });
        getContentPane().add(cboTipoServicio);
        cboTipoServicio.setBounds(130, 130, 100, 26);

        btnCerrar.setText("Cerrar");
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(340, 350, 86, 24);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("CALCULOS RECIBO DE LUZ"));

        jLabel9.setText("Subtotal");

        jLabel10.setText("Impuesto");

        jLabel11.setText("Total a Pagar");

        jLabel13.setText("$");

        jLabel14.setText("$");

        jLabel15.setText("$");

        lblSubtotal.setText("0");

        lblImpuesto.setText("0");

        lblTotalPago.setText("0");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(jLabel10)
                    .addComponent(jLabel9))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel15)
                    .addComponent(jLabel14)
                    .addComponent(jLabel13))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblSubtotal)
                    .addComponent(lblImpuesto)
                    .addComponent(lblTotalPago))
                .addGap(28, 28, 28)
                .addComponent(jLabel16)
                .addContainerGap(276, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel11))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(lblSubtotal))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(lblImpuesto))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(lblTotalPago)))))
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(30, 210, 438, 102);

        jLabel1.setText("Num. Recibo");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(30, 30, 72, 16);

        jLabel2.setText("Nombre");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(30, 70, 45, 16);

        jLabel3.setText("Domicilio");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(30, 100, 52, 16);

        jLabel4.setText("Costo por Kilowatts");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(240, 130, 107, 16);

        jLabel5.setText("Kilowatts Consumido");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(30, 170, 118, 16);

        jLabel7.setText("Tipo de Servicio");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(30, 130, 87, 16);

        txtFecha.setEnabled(false);
        getContentPane().add(txtFecha);
        txtFecha.setBounds(310, 30, 154, 24);

        btnGuarda.setText("Guardar");
        btnGuarda.setEnabled(false);
        btnGuarda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardaActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuarda);
        btnGuarda.setBounds(470, 60, 90, 42);

        txtNombre.setEnabled(false);
        getContentPane().add(txtNombre);
        txtNombre.setBounds(130, 60, 327, 24);

        btnNuevo.setText("Nuevo");
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(470, 20, 90, 34);

        txtNumRecibo.setEnabled(false);
        getContentPane().add(txtNumRecibo);
        txtNumRecibo.setBounds(130, 30, 74, 24);

        jLabel17.setText("Fecha:");
        getContentPane().add(jLabel17);
        jLabel17.setBounds(240, 30, 50, 16);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnGuardaActionPerformed

    private void cboTipoServicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTipoServicioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboTipoServicioActionPerformed

    private void cboTipoServicioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboTipoServicioItemStateChanged
       if(cboTipoServicio.getSelectedIndex()==1)
           txtCostoKilo.setText("2");
       else if(cboTipoServicio.getSelectedIndex()==2)
                txtCostoKilo.setText("3");
            else if(cboTipoServicio.getSelectedIndex()==3)
                txtCostoKilo.setText("5");
    }//GEN-LAST:event_cboTipoServicioItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dlgVIsta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dlgVIsta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dlgVIsta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dlgVIsta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                dlgVIsta dialog = new dlgVIsta(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnCancelar;
    public javax.swing.JButton btnCerrar;
    public javax.swing.JButton btnGuarda;
    public javax.swing.JButton btnLimpiar;
    public javax.swing.JButton btnMostrar;
    public javax.swing.JButton btnNuevo;
    public javax.swing.JComboBox<String> cboTipoServicio;
    public javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    public javax.swing.JLabel jLabel17;
    public javax.swing.JLabel jLabel2;
    public javax.swing.JLabel jLabel3;
    public javax.swing.JLabel jLabel4;
    public javax.swing.JLabel jLabel5;
    public javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    public javax.swing.JPanel jPanel1;
    public javax.swing.JLabel lblImpuesto;
    public javax.swing.JLabel lblSubtotal;
    public javax.swing.JLabel lblTotalPago;
    public javax.swing.JTextField txtCostoKilo;
    public javax.swing.JTextField txtDomicilio;
    public javax.swing.JTextField txtFecha;
    public javax.swing.JTextField txtKiloConsumido;
    public javax.swing.JTextField txtNombre;
    public javax.swing.JTextField txtNumRecibo;
    // End of variables declaration//GEN-END:variables
}
